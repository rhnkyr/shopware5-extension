<?php
namespace ENetworkersStockTaking\Subscriber;

use Enlight\Event\SubscriberInterface;

class Backend implements SubscriberInterface
{
    /**
     * @var string
     */
    private $pluginDirectory;

    /**
     * @var \Enlight_Template_Manager
     */
    private $templateManager;

    /**
     * @param $pluginDirectory
     * @param \Enlight_Template_Manager $templateManager
     */
    public function __construct($pluginDirectory, \Enlight_Template_Manager $templateManager)
    {
        $this->pluginDirectory = $pluginDirectory;
        $this->templateManager = $templateManager;
    }
    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            'Enlight_Controller_Action_PostDispatchSecure_Backend_ArticleList' => 'onArticleListPostDispatch'
        ];
    }

    public function onArticleListPostDispatch(\Enlight_Controller_ActionEventArgs $args)
    {
        /** @var \Shopware_Controllers_Backend_ArticleList $subject */
        $subject = $args->getSubject();
        $view = $subject->View();
        $request = $subject->Request();

        $view->addTemplateDir($this->pluginDirectory . '/Resources/views');
        

        if($request->getActionName() === 'load') {
            $view->extendsTemplate('backend/e_networkers_stock_taking/article_list/view/main/grid.js');
            $view->extendsTemplate('backend/e_networkers_stock_taking/article_list/controller/suggest.js');
            // $view->extendsTemplate('backend/e_networkers_stock_taking/article_list/controller/category_filter.js');
            // $view->extendsTemplate('backend/e_networkers_stock_taking/article_list/view/main/window.js');
        }
        // if($request->getActionName() === 'index') {
        //     $view->extendsTemplate('backend/e_networkers_stock_taking/app.js');
        // }
    }
}