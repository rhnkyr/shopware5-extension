// {block name="backend/article_list/view/main/grid"}
// {$smarty.block.parent}
// {include file="backend/e_networkers_stock_taking/view/price_summary.js"}

Ext.define('Shopware.apps.ENetworkersStockTaking.view.main.Grid', {
    override: 'Shopware.apps.ArticleList.view.main.Grid',

    getPagingbar: function () {
        var me = this;
        var parentItems = me.callParent(arguments);
        // load price_summary 
        this.priceSummary = Ext.create('Shopware.apps.ENetworkersStockTaking.view.priceSummary');
        // create an wrapper-container for bottom panel
        var items = Ext.create('Ext.container.Container', {
            items: [
                // inject price_summary
                this.priceSummary,
                // inject paging and other items
                parentItems
            ],
        });

        return items;
    }
});

// {/block}