/**
 *
 */
//{block name="backend/article_list/controller/suggest"}
//{$smarty.block.parent}
Ext.define('Shopware.apps.ENetworkersStockTaking.controller.Suggest', {
    override: 'Shopware.apps.ArticleList.controller.Suggest',

    loadFilter: function (filterString, name, extraParams) {

        this.callParent(arguments);

        const me = this;
        const mainWindow = this.getController('Main').mainWindow;
        const priceSummary = mainWindow.items.items[0].priceSummary;

        Ext.get('ek-display').dom.innerHTML = '';
        mainWindow.updateLayout();

        const filter = Ext.JSON.encode(me.getParser().getAst());

        Ext.Ajax.request({
            url: '{url controller=ENetworkersStockTaking action=getSummary}',
            params: {
                filter: filter
            },
            success: function (response, opts) {
                const obj = Ext.decode(response.responseText);

                if (obj.hasOwnProperty('success') && obj.success) {
                    priceSummary.renderResults(obj);
                }

            },
            error: function (response) {
                console.log(response);
            }
        });
    },

});
//{/block}