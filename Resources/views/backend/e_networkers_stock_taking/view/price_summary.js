// {literal}
Ext.define('Shopware.apps.ENetworkersStockTaking.view.priceSummary', {
    extend: 'Ext.container.Container',
    dock: 'bottom',
    items: [
        {
            xtype: 'displayfield',
            id: 'ek-display',
        }
    ],

    initComponent: function () {
        if (!this.items) {
            this.items = [];
        }

        this.callParent();

        Ext.util.CSS.createStyleSheet('#ek-display{width:100% !important;margin:0 !important}');

    },

    renderResults: function (data) {

        var html = '<table>\n' +
            '  <tr>\n' +
            '    <th style=" text-align: left;padding: 5px;font-weight: bold;width: 25%;border-bottom: 1px solid;">KOSTEN DER KATEGORIE</th>\n' +
            '    <th style=" text-align: left;padding: 5px;font-weight: bold;width: 40%;border-bottom: 1px solid;">KOSTEN DER KATEGORIE MINUS VERKAUFTE</th>\n' +
            '    <th style=" text-align: left;padding: 5px;font-weight: bold;width: 35%;border-bottom: 1px solid;">WERT & RESTWERT DER KATEGORIE</th>\n' +
            '  </tr>\n' +
            '  <tr>\n' +
            '    <td style=" text-align: left;padding: 5px;border-left: 1px solid;">EK: <b>' + data.category_total_ek + '</b></td>\n' +
            '    <td style=" text-align: left;padding: 5px;border-left: 1px solid;">EK: <b>' + data.category_after_minus_total_ek + '</b></td>\n' +
            '    <td style=" text-align: left;padding: 5px;border-left: 1px solid;">WERT: <b>' + data.category_total_price + '</b></td>\n' +
            '  </tr>\n' +
            '  <tr>\n' +
            '    <td style=" text-align: left;padding: 5px;border-left: 1px solid;">FEK: <b>' + data.category_total_fek + '</b></td>\n' +
            '    <td style=" text-align: left;padding: 5px;border-left: 1px solid;">FEK: <b>' + data.category_after_minus_total_fek + '</b></td>\n' +
            '    <td style=" text-align: left;padding: 5px;border-left: 1px solid;">RESTWERT DER KATEGORIE: <b>' + data.category_after_sold_total_price + '</b></td>\n' +
            '  </tr>\n' +
            '</table>';

        Ext.get('ek-display').dom.innerHTML = html;

        this.updateLayout();
    }
});
// {/literal}