<?php

use Shopware\Models\Article\Article;

class Shopware_Controllers_Backend_ENetworkersStockTaking extends Shopware_Controllers_Backend_Application
{
    protected $model = Article::class;
    protected $alias = 'article';

    public function getSummaryAction(): void
    {
        $request = $this->Request();
        /** @noinspection PhpUndefinedFieldInspection */
        $this->calcSummary($request->filter);
    }

    protected function calcSummary($filter, $httpCode = 200)
    {

        $this->Response()->setHeader('Content-type', 'application/json', true);

        if (empty($filter)) {
            $this->Response()->setHttpResponseCode($httpCode);
            return $this->Response()->setBody(json_encode(['success' => false, 'message' => 'Empty filter'], JSON_PRETTY_PRINT))->send();
        }

        try {

            $resource = $this->container->get('multi_edit.product');
            $result   = $resource->filter($filter, 0, 999999);

            //Attribute_enetArticleXtproductsBeginQuantity
            //Detail_inStock
            //Detail_active active passive status 1 - 0
            //Detail_purchasePrice => Purchase price
            //Price_price => Selling price

            $category_total_ek  = 0;
            $category_total_fek = 0;

            $category_after_minus_total_ek  = 0;
            $category_after_minus_total_fek = 0;

            $category_total_price      = 0;
            $category_sold_total_price = 0;

            foreach ($result['data'] as $product) {

                //KOSTEN DER KATEGORIE
                $category_total_fek += $product['Attribute_enetArticleXtproductsBeginQuantity'] * (float)$product['Attribute_enetArticleXtcfek'];
                $category_total_ek  += $product['Attribute_enetArticleXtproductsBeginQuantity'] * $product['Attribute_enetArticleXtcek'];

                //KOSTEN DER KATEGORIE MINUS VERKAUFTE
                $category_after_minus_total_fek += ($product['Attribute_enetArticleXtproductsBeginQuantity'] - $product['Detail_inStock']) * (float)$product['Attribute_enetArticleXtcfek'];
                $category_after_minus_total_ek  += ($product['Attribute_enetArticleXtproductsBeginQuantity'] - $product['Detail_inStock']) * $product['Attribute_enetArticleXtcek'];

                //WERT & RESTWERT DER KATEGORIE
                $category_total_price      += $product['Attribute_enetArticleXtproductsBeginQuantity'] * (float)$product['Price_price'];
                $category_sold_total_price += ($product['Attribute_enetArticleXtproductsBeginQuantity'] - $product['Detail_inStock']) * (float)$product['Price_price'];
            }

            setlocale(LC_MONETARY, "de_DE");

            return $this->Response()->setBody(
                json_encode(
                    [
                        'success'                         => true,
                        'category_total_ek'               => round($category_total_ek, 2),
                        'category_total_fek'              => round($category_total_fek, 2),
                        'category_after_minus_total_ek'   => round($category_total_ek - $category_after_minus_total_ek, 2),
                        'category_after_minus_total_fek'  => round($category_total_fek - $category_after_minus_total_fek, 2),
                        'category_total_price'            => money_format('%i', round($category_total_price, 2)),
                        'category_after_sold_total_price' => money_format('%i', round(($category_total_price - $category_sold_total_price), 2)),
                    ],
                    JSON_PRETTY_PRINT))
                ->send();

        } catch (Exception $ex) {
            $this->Response()->setHttpResponseCode(500);
            return $this->Response()->setBody(json_encode(['success' => false, 'message' => $ex->getMessage()], JSON_PRETTY_PRINT))->send();
        }

    }

}