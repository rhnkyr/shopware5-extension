<?php

namespace ENetworkersStockTaking\Tests;

use ENetworkersStockTaking\ENetworkersStockTaking as Plugin;
use Shopware\Components\Test\Plugin\TestCase;

class PluginTest extends TestCase
{
    protected static $ensureLoadedPlugins = [
        'ENetworkersStockTaking' => []
    ];

    public function testCanCreateInstance()
    {
        /** @var Plugin $plugin */
        $plugin = Shopware()->Container()->get('kernel')->getPlugins()['ENetworkersStockTaking'];

        $this->assertInstanceOf(Plugin::class, $plugin);
    }
}
