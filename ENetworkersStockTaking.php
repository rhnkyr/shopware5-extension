<?php

namespace ENetworkersStockTaking;

use Shopware\Components\Plugin;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Shopware-Plugin ENetworkersStockTaking.
 */
class ENetworkersStockTaking extends Plugin
{

    /**
    * @param ContainerBuilder $container
    */
    public function build(ContainerBuilder $container)
    {
        $container->setParameter('e_networkers_stock_taking.plugin_dir', $this->getPath());
        parent::build($container);
    }

}
